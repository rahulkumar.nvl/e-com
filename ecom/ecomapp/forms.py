from django import forms
from django.forms import ModelForm
from .models import Register
class LoginForm(forms.Form):
    username = forms.CharField(max_length=60)
    password = forms.CharField(max_length=12, widget=forms.PasswordInput)

class RegistrationForm(ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = Register
        fields = '__all__'
       