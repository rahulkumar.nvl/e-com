from django.db import models
import uuid 
from django.core.validators import RegexValidator
# Create your models here.
class Register(models.Model):
    id = models.UUIDField(primary_key=True, editable=False, default=uuid.uuid4)
    name = models.CharField(max_length=100, null=False)
    mobile_number_validate = RegexValidator(regex=r"^\+?1?\d{8,15}$")
    mobile_number = models.CharField(validators=[mobile_number_validate], max_length=16, blank=True, unique=True)
    email = models.EmailField(("email address"), unique=True, blank=True, null=True)
    password = models.CharField(max_length=100)
class Products(models.Model):
    id = models.URLField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=100)
    price = models.IntegerField()
    details = models.TextField()
    image = models.ImageField(upload_to='img/', blank=True, null=True)
    created_at = models.DateField(auto_now_add=True)

class Cart(models.Model):
    id = models.URLField(primary_key=True, default=uuid.uuid4, editable=False)
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    user = models.ForeignKey(Register, on_delete=models.CASCADE) 
    price = models.IntegerField()
    quantity = models.PositiveIntegerField()
    created_at = models.DateField(auto_now_add=True)

