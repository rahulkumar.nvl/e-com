from django.urls import path
from . import views
urlpatterns = [
    path('', views.registration, name="register"),
    path('login/', views.loginview, name="login"),
    path('home/', views.homeview, name="home"),
]